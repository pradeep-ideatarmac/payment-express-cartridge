'use strict';

/* API Includes */
var siteGenesisSiteName = 'PxSiteGenesis';
var Cart = require(siteGenesisSiteName + '_controllers/cartridge/scripts/models/CartModel');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');
var Helpers = require('~/cartridge/scripts/Helpers');

/* Script Modules */
var app = require(siteGenesisSiteName + '_controllers/cartridge/scripts/app');
var Logger = require('dw/system/Logger').getLogger('maxtest', 'maxtest');

function padLeft(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

/**
 * Verifies a credit card against a valid card number and expiration date and possibly invalidates invalid form fields.
 * If the verification was successful a credit card payment instrument is created.
 */

function Handle(args) {
    var cart = Cart.get(args.Basket);
    var creditCardForm = app.getForm('billing.paymentMethods.creditCard');
    var PaymentMgr = require('dw/order/PaymentMgr');

    var cardNumber = creditCardForm.get('number').value();
    var cardSecurityCode = creditCardForm.get('cvn').value();
    var cardType = creditCardForm.get('type').value();
    var expirationMonth = creditCardForm.get('expiration.month').value();
    var expirationYear = creditCardForm.get('expiration.year').value();
    var paymentCard = PaymentMgr.getPaymentCard(cardType);
    var creditCardStatus = paymentCard.verify(expirationMonth, expirationYear, cardNumber, cardSecurityCode);

    if (creditCardStatus.error) {

        var invalidatePaymentCardFormElements = require(siteGenesisSiteName + '_core/cartridge/scripts/checkout/InvalidatePaymentCardFormElements');
        invalidatePaymentCardFormElements.invalidatePaymentCardForm(creditCardStatus, session.forms.billing.paymentMethods.creditCard);

        return {error: true};
    }
 
    var customer = args.Basket.getCustomer();
    
    return Transaction.wrap(function () {
    	cart.removeExistingPaymentInstruments(args.PaymentMethodID);
		var paymentInstrument = cart.createPaymentInstrument(args.PaymentMethodID, cart.getNonGiftCertificateAmount());
		
        paymentInstrument.creditCardHolder = creditCardForm.get('owner').value();
        paymentInstrument.creditCardNumber = cardNumber;
        paymentInstrument.creditCardType = cardType;
        paymentInstrument.creditCardExpirationMonth = expirationMonth;
        paymentInstrument.creditCardExpirationYear = expirationYear;

        var pxExpirationMonth = padLeft("" + expirationMonth, 2);
        var pxExpirationYear = "" + expirationYear;
        pxExpirationYear = pxExpirationYear[2] + pxExpirationYear[3];
        var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();
        
        var Txn = paymentInstrument.getPaymentTransaction(); //class PaymentTransaction in demandware
        var amount = Txn.getAmount();
        
		//auth/complete is not a good idea as user will likely be pressing...
        //...refresh a dozen times on the next screen or...
        //...going back and forth in the browser. so go with tokenize/purchase or could do tokenize/auth and then complete later by merchant
    	var CardNumber2 = Helpers.Tokenize(paymentProcessor, paymentInstrument, cardSecurityCode, pxExpirationMonth + pxExpirationYear);

    	if(CardNumber2 != null) {
			if(customer.registered) {
				if(customer.profile.custom.PxCardNumber2s == null)
					customer.profile.custom.PxCardNumber2s = " "; //why space? javascript weirdness...
					
				if(("" + customer.profile.custom.PxCardNumber2s).indexOf(CardNumber2 + ",") == -1)
					customer.profile.custom.PxCardNumber2s += paymentInstrument.maskedCreditCardNumber + "=" + CardNumber2 + ",";
				
				Logger.error('Customer CardNumber2s: {0}', customer.profile.custom.PxCardNumber2s);
			}
			
			Txn.custom.PxCardNumber2 = CardNumber2;
			
			return {success: true };
    	}
    	 
    	 return {error: true};
    });
}

/**
 * Authorizes a payment using a credit card. The payment is authorized by using the BASIC_CREDIT processor
 * only and setting the order no as the transaction ID. Customizations may use other processors and custom
 * logic to authorize credit card payment.
 */
function Authorize(args) {
    var order = args.Order;
	var orderNo = args.OrderNo;
    var paymentInstrument = args.PaymentInstrument;
    var Txn = paymentInstrument.getPaymentTransaction();
    var amount = Txn.getAmount();
    var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();

    Transaction.wrap(function () {
        paymentInstrument.paymentTransaction.transactionID = orderNo;
        paymentInstrument.paymentTransaction.paymentProcessor = paymentProcessor;
    });
    
    var CardNumber2 = Txn.custom.PxCardNumber2
    
    var data : XML = 
		<Txn>
			<PostUsername>{paymentProcessor.getPreferenceValue("PxPostUsername")}</PostUsername>
			<PostPassword>{paymentProcessor.getPreferenceValue("PxPostPassword")}</PostPassword>
			<TxnType>{Helpers.EstablishTxnType(paymentProcessor)}</TxnType>
			<CardNumber2>{Txn.custom.PxCardNumber2}</CardNumber2>
			<Amount>{amount.toNumberString()}</Amount>
			<InputCurrency>{amount.getCurrencyCode()}</InputCurrency>
			<ReceiptEmail>{order.getCustomerEmail()}</ReceiptEmail>
		</Txn>;
		
	return Transaction.wrap(function() {
		var XmlResponse = Helpers.SendHttpPxPost(paymentProcessor, data);
		
		if(XmlResponse == null)
			return {error : true};
			
		Txn.custom.DpsTxnRef = XmlResponse.DpsTxnRef;	
		Txn.custom.PxXmlResponse = XmlResponse; //this is a string, not XML type. so assign only now.
		
		var placeOrderStatus = OrderMgr.placeOrder(order);
	    if (placeOrderStatus === dw.system.Status.ERROR) {
	        OrderMgr.failOrder(order);
        	return {error : true};
	    }
	    
		order.setStatus(dw.order.Order.ORDER_STATUS_COMPLETED);
		order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
		
		return {authorized: true};
	});
}

/*
 * Module exports
 */

/*
 * Local methods
 */
exports.Handle = Handle;
exports.Authorize = Authorize;

