'use strict';

var siteGenesisSiteName = 'PxSiteGenesis';
var Cart = require(siteGenesisSiteName + '_controllers/cartridge/scripts/models/CartModel');
var app = require(siteGenesisSiteName + '_controllers/cartridge/scripts/app');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var OrderMgr = require('dw/order/OrderMgr');
var Helpers = require('~/cartridge/scripts/Helpers');

var Logger = require('dw/system/Logger').getLogger('maxtest', 'maxtest');

/*function enumerate(node) {
	for(var nodelet in node) {
			Logger.error('test: {0} {1}', nodelet, node[nodelet]);
			enumerate(node[nodelet]);
	}
}*/

function Handle(args) {
	var cart = Cart.get(args.Basket);
	
	Transaction.wrap(function() {
		cart.removeExistingPaymentInstruments(args.PaymentMethodID);
		var paymentInstrument = cart.createPaymentInstrument(args.PaymentMethodID, cart.getNonGiftCertificateAmount());

		//the DpsBillingId/token is passed in via the cardnumber field (hack).  
		paymentInstrument.getPaymentTransaction().custom.PxDpsBillingId = session.forms.billing.paymentMethods.creditCard.number.value; 
	});

    return {success: true};
}

function Authorize(args) {
	//note: we come here from PaymentProcessorModel.authorize 
	var order = args.Order;
	var orderNo = args.OrderNo;
	var paymentInstrument = args.PaymentInstrument;
	var paymentProcessor = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod()).getPaymentProcessor();
	var urlCallback = dw.web.URLUtils.https('PxPay-Submit').append("order_id", orderNo).append("order_token", order.getOrderToken()).append("payment_processor", paymentProcessor.ID);
	
	var preferenceValuePrefix = Helpers.GetPaymentProcessorPrefixHack(paymentProcessor); 
	//note: order_token makes the UrlSuccess secure. No need for extra encryption layer etc.
	var data : XML = 
		<GenerateRequest>
			<PxPayUserId>{paymentProcessor.getPreferenceValue(preferenceValuePrefix + "UserId")}</PxPayUserId>
			<PxPayKey>{paymentProcessor.getPreferenceValue(preferenceValuePrefix + "Key")}</PxPayKey>
			<AmountInput>{order.getTotalGrossPrice().toNumberString()}</AmountInput>
			<TxnType>{Helpers.EstablishTxnType(paymentProcessor)}</TxnType>
			<CurrencyInput>{order.getCurrencyCode()}</CurrencyInput>
			<UrlSuccess>{urlCallback}</UrlSuccess>
			<UrlFail>{urlCallback}</UrlFail>
			<EmailAddress>{order.getCustomerEmail()}</EmailAddress>		
			<MerchantReference>{orderNo}</MerchantReference>
			<TxnData1>{"Demandware Transaction"}</TxnData1>
			<TxnData2>{paymentProcessor.ID}</TxnData2>
			<TxnData3>{order.getCustomerNo()}</TxnData3>
			<ClientVersion>{"0.0.1"}</ClientVersion>
		</GenerateRequest>; 	
	
	Helpers.AddAddressDetailsToXml(data, order);
	
	var Txn = paymentInstrument.getPaymentTransaction();
	
	if(Txn.custom.PxDpsBillingId != null && Txn.custom.PxDpsBillingId.length)
		data.DpsBillingId = Txn.custom.PxDpsBillingId;
	else if(paymentProcessor.getPreferenceValue(preferenceValuePrefix + "EnableAddBillCard")) {
		data.EnableAddBillCard = '1';
		//can put custom code for adding a BillingId here (there's a BillingId which can be merchant supplied, and there's always a DpsBillingId (auto generated)).
		//the default I'll put in will just use orderNo (which is the same as merchant reference).
		data.BillingId = orderNo;
	}

	return Transaction.wrap(function() {
		
		var XmlResponse = Helpers.SendHttpPxPay(paymentProcessor, data);

		if(XmlResponse == null)
			return {error:true};

		if(paymentProcessor.getPreferenceValue(preferenceValuePrefix + "UseIFrame")) {
			var iFrameInternalUrl = dw.web.URLUtils.https('PxPay-IFrame').append("URI", XmlResponse.URI);
			var style = "width:100%;text-align:center;";
			//an alternative to this tactic, is to actually force redirect for mobile. might look better?
			if(Helpers.IsMobile() == false)
				style += "padding-left:15%;";
			
			iFrameInternalUrl.append("style", style);
			
			response.redirect(iFrameInternalUrl);
		}
		else
			response.redirect(XmlResponse.URI);
		
		return {authorized: true}; 
	});
}

exports.Handle = Handle
exports.Authorize = Authorize;

