var Transaction = require('dw/system/Transaction');
var Logger = require('dw/system/Logger').getLogger('maxtest', 'maxtest');
var OrderMgr = require('dw/order/OrderMgr');


function getPaymentProcessorPrefixHack(paymentProcessor) {
	 return paymentProcessor.ID.replace("PAYMENT_EXPRESS_", ""); //nasty hack. store the settings (pxpayuserid, pxpaykey etc for the api user under the return value of this function).
}


function sendHttp(validateResponse) {
	return function(endPoint, data) {
		Logger.error("XmlRequest:\n\n {0}", data.toString());
		
		try {	
			var httpClient = new dw.net.HTTPClient();  
			httpClient.timeout = 3000;  
			httpClient.open('POST', endPoint);  
			httpClient.send(data);  
			
			if (httpClient.statusCode == 200) 
		    { 
		    	var XmlResponse = new XML(httpClient.text);   	
				
				return validateResponse(XmlResponse);
			}  
		    else
		    	Logger.error("Error processing response. httpClient.statusCode {0} ", httpClient.statusCode);
		}
		catch(Exception) {
			Logger.error("Exception sending Http. Wrong endpoint? Endpoint down? Endpoint: {0} Exception: {1}", endPoint, Exception);
		}
		
		return null;
	}
}

var sendHttpPxPostXml = sendHttp(function(XmlResponse) {
	Logger.error("XmlResponse PxPost:\n\n {0}", XmlResponse.toString());
	
	try {
		if(XmlResponse.Transaction.Authorized == '1' && XmlResponse.DpsTxnRef != null)
			return XmlResponse;
	}
	catch(e) {}
	
	return null;
});

exports.SendHttpPxPost = function(paymentProcessor, data) {
	return sendHttpPxPostXml(paymentProcessor.getPreferenceValue("PxPostEndPoint"), data.toString());
} 

exports.Tokenize = function(paymentProcessor, paymentInstrument, cardSecurityCode, expiryDate, cardHolderName) {
	//note: no charge (no auth) will be applied. amount 0.01 has to be sent for some legacy reasons. same goes for input currency.
	var data : XML = 
		<Txn>
    		<PostUsername>{paymentProcessor.getPreferenceValue("PxPostUsername")}</PostUsername>
			<PostPassword>{paymentProcessor.getPreferenceValue("PxPostPassword")}</PostPassword>
	    	<TxnType>Tokenize</TxnType>
	    	<CardNumber>{paymentInstrument.creditCardNumber}</CardNumber>
	    	<DateExpiry>{expiryDate}</DateExpiry>
			<Cvc2>{cardSecurityCode}</Cvc2>
			<CardHolderName>{paymentInstrument.creditCardHolder}</CardHolderName>
	    	<Amount>0.01</Amount>
	    	<InputCurrency>NZD</InputCurrency>
	    </Txn>;
	    
	var XmlResponse = exports.SendHttpPxPost(paymentProcessor, data.toString());
	if(XmlResponse != null && XmlResponse.Transaction.CardNumber2 != null)
		return XmlResponse.Transaction.CardNumber2;
	return null;
} 

var sendHttpPxPayXml = sendHttp(function(XmlResponse) {
	Logger.error("XmlResponse PxPay:\n\n {0}", XmlResponse.toString());
	
	if(XmlResponse.URI != null) 
		return XmlResponse;

	return null;
});

exports.SendHttpPxPay = function(paymentProcessor, data) {
	return sendHttpPxPayXml(paymentProcessor.getPreferenceValue("PxPayEndPoint"), data.toString());
} 
	

exports.GetPxPayResult = function(paymentProcessor, token) {
	var preferenceValuePrefix = getPaymentProcessorPrefixHack(paymentProcessor);
	
	var data : XML =
	 <ProcessResponse>
		<PxPayUserId>{paymentProcessor.getPreferenceValue(preferenceValuePrefix + "UserId")}</PxPayUserId>
		<PxPayKey>{paymentProcessor.getPreferenceValue(preferenceValuePrefix + "Key")}</PxPayKey>
		<Response>{token}</Response>
	</ProcessResponse>;
	
	return sendHttp(function(XmlResponse) {
		Logger.error("XmlResponse PxPay Status:\n\n {0}", XmlResponse.toString());
		return XmlResponse;
	})(paymentProcessor.getPreferenceValue("PxPayEndPoint"), data.toString());
} 

exports.SetOrderSuccess = function(order, XmlResponse) { 
	return Transaction.wrap(function() {

		if(XmlResponse.Success) {
			if(XmlResponse.ReCo == "00") {
				if(XmlResponse.TxnType == "Purchase")
					order.setPaymentStatus(dw.order.Order.PAYMENT_STATUS_PAID);
				order.setStatus(dw.order.Order.ORDER_STATUS_COMPLETED);
				return true;
			}
			else {
				Logger.error("Order declined. Px ReCo: {0}", XmlResponse.ReCo);
				order.setStatus(dw.order.Order.ORDER_STATUS_CANCELLED);
			}
		} 

		return OrderMgr.failOrder(order).error == false;   			
	});
}

exports.GetOrderIsInProgress = function(order) {
	return order.getStatus() == dw.order.Order.ORDER_STATUS_NEW || order.getStatus() == dw.order.Order.ORDER_STATUS_CREATED;
};

exports.EstablishTxnType = function(paymentProcessor) {
	var preferenceValuePrefix = getPaymentProcessorPrefixHack(paymentProcessor); //note: Account2Account can only do Purchase.
	return paymentProcessor.getPreferenceValue(preferenceValuePrefix + "UseAuthComplete") ? "Auth" : "Purchase";
};
	
function truncateToMaxLength(str, maxLength) {
	if(str != null)
		return str.substring(0, maxLength);
	
	return "";
}

exports.AddAddressDetailsToXml = function(XmlCommand, order) {
	var address = order.getBillingAddress();
	//don't think dw XML class nicely supports dynamic tag names ...
	//e.g. cannot do something like XmlCommand. Type + CountryCode = address.countryCode where Type is a string variable (either 'Shipping' or 'Billing')...
	//...so just copy paste then find and replace the below if statement for the shipping and billing parts...
	//not for the phonenumber, guess the shipping can just overwrite the billing? we can only ever use one of them really.

	//copy+paste begins
	if(address != null) {
		var phone = address.phone.replace("-", "").replace("-", "");
		XmlCommand.PhoneNumber = phone; //phone number for risk management purposes (if applicable)
		
		XmlCommand.BillingName = address.fullName;

		if(address.address1 != null)  {
			XmlCommand.BillingAddress = address.address1;
			if(address.address2 != null)
				XmlCommand.BillingAddress += ", " + address.address2;
		}
			
		XmlCommand.BillingPostalCode = address.postalCode;	
		if(address.phone != null)
			XmlCommand.BillingPhoneNumber = phone;
		XmlCommand.BillingCountryCode = address.countryCode;

		XmlCommand.BillingName = truncateToMaxLength(XmlCommand.BillingName, 128); //should be fine. just in case
		XmlCommand.BillingAddress = truncateToMaxLength(XmlCommand.BillingAddress, 128); //should be fine. just in case
		XmlCommand.BillingPostalCode = truncateToMaxLength(XmlCommand.BillingPostalCode, 10); //should be fine. just in case
		XmlCommand.BillingPhoneNumber = truncateToMaxLength(XmlCommand.BillingPhoneNumber, 10); //should be fine. just in case.
		XmlCommand.BillingCountryCode = truncateToMaxLength(XmlCommand.BillingCountryCode, 3); //should be fine. demandware sends 2 digit ISO
	}
	//copy+paste ends

	address = order.shippingOrders.length ? order.shippingOrders[0].shippingAddress : null;
	
	//copy+paste begins
	if(address != null) {
		var phone = address.phone.replace("-", "").replace("-", "");
		XmlCommand.PhoneNumber = phone; //phone number for risk management purposes (if applicable)
		
		XmlCommand.ShippingName = address.fullName;

		if(address.address1 != null)  {
			XmlCommand.ShippingAddress = address.address1;
			if(address.address2 != null)
				XmlCommand.ShippingAddress += ", " + address.address2;
		}
			
		XmlCommand.ShippingPostalCode = address.postalCode;	
		if(address.phone != null)
			XmlCommand.ShippingPhoneNumber = phone;
		XmlCommand.ShippingCountryCode = address.countryCode;

		XmlCommand.ShippingName = truncateToMaxLength(XmlCommand.ShippingName, 128); //should be fine. just in case
		XmlCommand.ShippingAddress = truncateToMaxLength(XmlCommand.ShippingAddress, 128); //should be fine. just in case
		XmlCommand.ShippingPostalCode = truncateToMaxLength(XmlCommand.ShippingPostalCode, 10); //should be fine. just in case
		XmlCommand.ShippingPhoneNumber = truncateToMaxLength(XmlCommand.ShippingPhoneNumber, 10); //should be fine. just in case.
		XmlCommand.ShippingCountryCode = truncateToMaxLength(XmlCommand.ShippingCountryCode, 3); //should be fine. demandware sends 2 digit ISO
	}
	//copy+paste ends
	
};

exports.GetPaymentProcessorPrefixHack = getPaymentProcessorPrefixHack;

function addCardTokenForCustomer(customer, customerTokens, token, maskedCreditCardNumber) {
	//hacky as hell, but could not figure out how to use those "set of strings" variables...
	//Transaction.wrap must be called higher up the call stack
	if(token == null || !customer.registered) 
		return {error: true};
	
	if(customerTokens == null)
		customerTokens = " "; //why space? javascript weirdness...
		
	var index = ("" + customerTokens).indexOf(maskedCreditCardNumber + "=");
	//if already present, then update it (by deleting and then re adding)
	if(index != -1) {
		//remove number of chars = maskedCreditCardNumber.length + '='.length + length of DpsBillingId/CardNumber2 (always 16) + ','.length
		customerTokens = customerTokens.slice(0, index)
			+ customerTokens.slice(index + maskedCreditCardNumber.length + 1 + 16 + 1, customerTokens.length);
	}
		
	customerTokens += maskedCreditCardNumber + "=" + token + ",";
	Logger.error('Customer tokens: {0}', customerTokens);
	return customerTokens;
}

exports.AddDpsBillingId = function(customer, XmlResponse) {
	return Transaction.wrap(function() {
		var result = addCardTokenForCustomer(customer, customer.profile.custom.PxDpsBillingIds, XmlResponse.DpsBillingId, XmlResponse.CardNumber);
		if(!result.error)
			customer.profile.custom.PxDpsBillingIds = result;
		return result;
	});	
};

exports.AddCardNumber2 = function(customer, XmlResponse) {
	return Transaction.wrap(function() {
		var result = addCardTokenForCustomer(customer, customer.profile.custom.PxCardNumber2s, XmlResponse.CardNumber2, XmlResponse.CardNumber);
		if(!result.error)
			customer.profile.custom.PxCardNumber2s = result;
		return result;
	});	
};

exports.IsMobile = function() {
	var mobileAgentHash = ["mobile","tablet","phone","ipad","ipod","android","blackberry","windows ce","opera mini","palm"],
	idx = 0,
	item = null,
	isMobile = false,
	userAgent = request.httpUserAgent.toLowerCase();

	while (item = mobileAgentHash[idx++] && !isMobile) 
	  isMobile = (userAgent.indexOf(mobileAgentHash[idx]) >= 0);

	return isMobile;
};



