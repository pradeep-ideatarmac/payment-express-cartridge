'use strict';
var siteGenesisSiteName = 'PxSiteGenesis';
var guard = require(siteGenesisSiteName + '_controllers/cartridge/scripts/guard');
var app = require(siteGenesisSiteName + '_controllers/cartridge/scripts/app');
var OrderMgr = require('dw/order/OrderMgr');
var PaymentMgr = require('dw/order/PaymentMgr');
var Transaction = require('dw/system/Transaction');
var Helpers = require('~/cartridge/scripts/Helpers');
var Logger = require('dw/system/Logger').getLogger('maxtest', 'maxtest');

function submit() {
	var order = OrderMgr.getOrder(request.httpParameterMap.order_id.stringValue);
	
	if(order == null)
		return; //shouldn't happen with unmodified urlsuccess/fail
	
	if(request.httpParameterMap.order_token.stringValue !== order.getOrderToken())
		return; //ditto null check
	
	var paymentProcessor = PaymentMgr.getPaymentMethod(request.httpParameterMap.payment_processor.stringValue).getPaymentProcessor(); 
	var XmlResponse = Helpers.GetPxPayResult(paymentProcessor, request.httpParameterMap.result.stringValue);
	
    if (Helpers.GetOrderIsInProgress(order)) {
		var orderPlacementStatus = null;
	    
	    if(XmlResponse == null) 
	    	Logger.error("PxPayStatus (ProcessRequest) request failed!"); //no need to fail order. just wait for another FPRN etc.    	
     	else if(Helpers.SetOrderSuccess(order, XmlResponse)) {}
	    
		//clearForms();	//not sure what this is for 
	    
	    if(order.getStatus() == dw.order.Order.ORDER_STATUS_COMPLETED) {
	    	Helpers.AddDpsBillingId(order.getCustomer(), XmlResponse);
	    	Helpers.AddCardNumber2(order.getCustomer(), XmlResponse);
	    }
    }
    
    var isFPRN = request.getHttpReferer() == null; //check could be tighter.   
    if(isFPRN) {
    	//this came from the FPRN uplink (asynchronous notification)
		//if order is still in progress at this point, than that means something went wrong and the info in the...
		//...FPRN was not sufficient to determine order success/fail status (likely something went wrong doign GetPxPayResult)...
		//...So we need to tell FPRN uplink to keep sending us FPRNs. Do this with a status code, which can then be configured as a FAIL code in PxPayPostConfig  	
    	//428 == 'Precondition Required'. Probably makes sense for this
    	var status : Number = Helpers.GetOrderIsInProgress(order) ? 428 : 200;
    	response.setStatus(status);
    	Logger.error("Sending FPRN Response {0}", status);
    	return;
	}
    
    if(order.getStatus() == dw.order.Order.ORDER_STATUS_COMPLETED)
    	return app.getController('COSummary').ShowConfirmation(order);
     
    var declinedScreen = dw.web.URLUtils.https('PxPay-Declined');
    var ReCo = "ER";
    var ResponseText  ="Error";
    if(XmlResponse && XmlResponse.ReCo)
    	ReCo = XmlResponse.ReCo;
    if(XmlResponse && XmlResponse.ResponseText)
    	ResponseText = XmlResponse.ResponseText;
    
    declinedScreen.append("ReCo", ReCo).append("ResponseText", ResponseText);
    response.redirect(declinedScreen);
    return;
}

exports.Submit = guard.ensure(['https'], submit); 

function iFrame()  {
	app.getView().render('PxPay_IFRAME');
};

exports.IFrame = guard.ensure(['https'], iFrame); 

function declined()  {
	app.getView().render('Px_Declined');
};

exports.Declined = guard.ensure(['https'], declined); 
