'use strict';

var ArrayList = require('dw/util/ArrayList');
var List = require('dw/util/List');
var PaymentInstrument = require('dw/order/PaymentInstrument');
var PaymentMgr = require('dw/order/PaymentMgr');
var Logger = require('dw/system/Logger').getLogger('maxtest', 'maxtest');

/**
 * Validates payment instruments and returns valid payment instruments.
 *
 * @alias module:models/ProfileModel~ProfileModel/validateWalletPaymentInstruments
 * @param {dw.customer.Wallet|dw.order.Basket} paymentContainer - Entity that possesses payment instruments
 * @param {String} countryCode Billing country code or null.
 * @param {Number} amount Payment amount to check valid payment instruments for.
 * @returns {ArrayList} Returns an array with the valid PaymentInstruments.
 */
function validatePaymentInstruments(paymentContainer, countryCode, amount) {

    var paymentInstruments = paymentContainer.getPaymentInstruments();

    // Gets applicable payment methods.
    var methods = PaymentMgr.getApplicablePaymentMethods(customer, countryCode, amount);

    // Gets applicable payment cards from CREDIT_CARD payment method.
    var creditCardMethod = PaymentMgr.getPaymentMethod(PaymentInstrument.METHOD_CREDIT_CARD);
    var cards = creditCardMethod ? creditCardMethod.getApplicablePaymentCards(customer, countryCode, amount) : List.EMPTY_LIST;

    // Collects all invalid payment instruments.
    var validPaymentInstruments = new ArrayList(paymentInstruments);
    var invalidPaymentInstruments = new ArrayList();
    //Logger.error('payment instrument count: {0} cards count: {1} cards[0] = {2}', paymentInstruments.length, cards.length, cards.length ? cards[0].name : ''   );
    
    for (var i = 0; i < paymentInstruments.length; i++) {
        var paymentInstrument = paymentInstruments[i];

        // Ignores gift certificate payment instruments.
        if (PaymentInstrument.METHOD_GIFT_CERTIFICATE.equals(paymentInstrument.paymentMethod)) {
            continue;
        }
        
        //Logger.error('payment instrument: {0}', paymentInstrument.paymentMethod   );

        // Gets a payment method.
        var method = PaymentMgr.getPaymentMethod(paymentInstrument.getPaymentMethod());

        // Checks whether payment method is still applicable.
        if (method && methods.contains(method)) {
            // In case of method CREDIT_CARD, check payment cards
            if (PaymentInstrument.METHOD_CREDIT_CARD.equals(paymentInstrument.paymentMethod)) {
                // Gets payment card.
                var card = PaymentMgr.getPaymentCard(paymentInstrument.creditCardType);
                // Checks whether payment card is still applicable.
                if (card && cards.contains(card)) {
                    continue;
                }
            } else {
                // Continues if method is applicable.
                continue;
            }
        }

        // Collects invalid payment instruments.
        invalidPaymentInstruments.add(paymentInstrument);
        validPaymentInstruments.remove(paymentInstrument);
    }

    //Logger.error("validPaymentInstruments.length = {0}", validPaymentInstruments.length);
    if (invalidPaymentInstruments.size()) {
        return {
            InvalidPaymentInstruments: invalidPaymentInstruments,
            ValidPaymentInstruments: validPaymentInstruments
        };
    } else {
        return {
            ValidPaymentInstruments: validPaymentInstruments
        };
    }
}

module.exports = {
    validatePaymentInstruments: validatePaymentInstruments
};
